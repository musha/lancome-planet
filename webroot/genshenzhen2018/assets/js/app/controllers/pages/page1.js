// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            // currentPage: 0,
            scroll: null,
            defaultOrientation: "portrait", // portrait, landscape, custom
            layoutOrientation: "custom", // portrait, landscape, custom
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/ui/iscroll", "vendor/zepto/zepto.html5Loader.min","vendor/zepto/fx", "vendor/zepto/fx_methods"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                gtag('config', 'UA-84868999-18', {'page_title': 'genshenzhen_p2' });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                // android还是ios
                var u = navigator.userAgent;
                app.isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
                app.isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

                app.planetNum = '';
                app.isSelect = 0;

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.page1_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                    starFalling();
                    tipTwinkle();
                    // arrowTwinkle();
                } }, 0.1);
                tl.to(context.$('.planet3-light'), 0.1, { onComplete: function() {
                    planetTwinkle(context.$('.planet3-light'));
                }});
                tl.to(context.$('.planet2-light'), 0.1, { onComplete: function() {
                    planetTwinkle(context.$('.planet2-light'));
                }});
                tl.to(context.$('.planet4-light'), 0.1, { onComplete: function() {
                    planetTwinkle(context.$('.planet4-light'));
                }});
                tl.to(context.$('.planet1-light'), 0.1, { onComplete: function() {
                    planetTwinkle(context.$('.planet1-light'));
                }});
                tl.to(context.$('.planet5-light'), 0.1, { onComplete: function() {
                    planetTwinkle(context.$('.planet5-light'));
                }});

                // 提示
                function tipTwinkle() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.tip'), 0.4, { autoAlpha: 0 }, '+=1');
                    tl.to(context.$('.tip'), 1., { autoAlpha: 1 });
                    tl.to(context.$('.tip'), 0.4, { autoAlpha: 0 });
                    tl.to(context.$('.tip'), 1., { autoAlpha: 1 });
                    tl.to(context.$('.tip'), 0.4, { autoAlpha: 0 });
                };

                // 箭头 
                function arrowTwinkle() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.arrows'), 0.4, { autoAlpha: 0 }, '+=4.5');
                    tl.to(context.$('.arrows'), 0.4, { autoAlpha: 1 });
                    tl.to(context.$('.arrows'), 0.4, { autoAlpha: 0 });
                    tl.to(context.$('.arrows'), 0.4, { autoAlpha: 1 });
                    tl.to(context.$('.arrows'), 0.4, { autoAlpha: 0 });
                };

                // 星球闪烁
                function planetTwinkle(el) {
                    var tl = new TimelineMax({repeat: -1, yoyo: true, repeatDelay: 0.1 });
                    tl.to(el, 1.2, { autoAlpha: 0 });
                };

                // 流星划过
                function starFalling() {
                    var tl = new TimelineMax({ repeat: -1 });
                    tl.to(context.$('.star6'), 8, { autoAlpha: 0.4, top: '7%', left: '69.6%', repeatDelay: 3, onComplete: function() {
                        context.$('.star6').animate({'opacity': 0}, 800);
                    } }, 0.1);
                    tl.to(context.$('.star5'), 4, { autoAlpha: 1, top: '78.6%', left: '48%', repeatDelay: 2, onComplete: function() {
                        context.$('.star5').animate({'opacity': 0}, 400);
                    } }, 0.3);
                    tl.to(context.$('.star7'), 5, { autoAlpha: 1, top: '80.6%', left: '71%', repeatDelay: 4,  onComplete: function() {
                        context.$('.star7').animate({'opacity': 0}, 100);
                    } }, 0.1);
                    tl.to(context.$('.star4'), 3, { autoAlpha: 1, top: '12%', left: '33.6%', repeatDelay: 2, onComplete: function() {
                        context.$('.star4').animate({'opacity': 0}, 300);
                    } }, 0.3);
                    tl.to(context.$('.star3'), 6, { autoAlpha: 1, top: '-3.6%', left: '35.2%', repeatDelay: 3, onComplete: function() {
                        context.$('.star3').animate({'opacity': 0}, 500);
                    } }, 0.1);
                    tl.to(context.$('.star2'), 6, { autoAlpha: 1, top: '67.8%', left: '29.1%', repeatDelay: 4, onComplete: function() {
                        context.$('.star2').animate({'opacity': 0}, 400);
                    } }, 0.3);
                    tl.to(context.$('.star1'), 8, { autoAlpha: 0.4, top: '79.8%', left: '18.2%', repeatDelay: 3,  onComplete: function() {
                        context.$('.star1').animate({'opacity': 0}, 800);
                    } }, 0.1);
                    tl.to(context.$('.star8'), 7, { autoAlpha: 0.6, top: '15%', left: '93.7%', repeatDelay: 2, onComplete: function() {
                        context.$('.star8').animate({'opacity': 0}, 200);
                    } }, 0.3);
                    tl.to(context.$('.star9'), 3, { autoAlpha: 1, top: '16.9%', left: '91.1%', repeatDelay: 4, onComplete: function() {
                        context.$('.star9').animate({'opacity': '0'}, 300);
                    } }, 0.1);
                    tl.to(context.$('.star10'), 2, { autoAlpha: 1, top: '87.6%', left: '89.8%', repeatDelay: 4,  onComplete: function() {
                        context.$('.star10').animate({'opacity': 0}, 100);
                    } }, 0.3);
                    tl.to(context.$('.star11'), 5, { autoAlpha: 0.4, top: '-6.4%', left: '106.4%', repeatDelay: 3, onComplete: function() {
                        context.$('.star11').animate({'opacity': 0}, 500);
                    } }, 0.5);
                };

                //重力旋转
                this.DeviceMotion();

                // 默认事件
                // var defaultEvent = function(e) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // };
                // 暂时取消滑动事件
                $('body').on('touchmove', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                });
                $('document').on('touchmove', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                });
                $('#main').on('touchmove', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                });

                // 是否为整数
                function isInteger(obj) {
                    return obj%1 === 0;
                };

                // 向左滑动
                $('.arrow.left').on('click', function() {
                    var that = $(this);
                    // 选定了星球就不能滑动
                    if(app.isSelect == 1) {
                        return;
                    };
                    // 当前left
                    var currentLeft = parseFloat($('.page1_page').css('left'));
                    if($('.arrow.right').css('display') == 'none') {
                        $('.arrow.right').fadeIn(150);
                    };
                    if(currentLeft == 0) {
                        return;
                    } else {
                        app.isSelect = 1;
                        // 是否为整数
                        if(isInteger(currentLeft)) {
                            $('.page1_page').animate({'left': currentLeft + 100 + '%' }, 500, function() {
                                app.isSelect = 0;
                                if(parseFloat($('.page1_page').css('left')) == 0) {
                                    that.fadeOut(150);
                                };
                            });
                        } else {
                            var times = parseInt(currentLeft/100);
                            $('.page1_page').animate({'left': times * 100 + '%' }, 500, function() {
                                app.isSelect = 0;
                                if(parseFloat($('.page1_page').css('left')) == 0) {
                                    that.fadeOut(150);
                                };
                            });
                        };
                    };
                });

                // 向右滑动
                $('.arrow.right').on('click', function() {
                    var that = $(this);
                    // 选定了星球就不能滑动
                    if(app.isSelect == 1) {
                        return;
                    };
                    // 当前left
                    var currentLeft = parseFloat($('.page1_page').css('left'));
                    if($('.arrow.left').css('display') == 'none') {
                        $('.arrow.left').fadeIn(150);
                    };
                    if(currentLeft == -400) {
                        return;
                    } else {
                        app.isSelect = 1;
                        // 是否为整数
                        if(isInteger(currentLeft)) {
                            $('.page1_page').animate({'left': currentLeft - 100 + '%' }, 500, function() {
                                app.isSelect = 0;
                                if(parseFloat($('.page1_page').css('left')) == -400) {
                                    that.fadeOut(150);
                                };
                            });
                        } else {
                            var times = parseInt(currentLeft/100) - 1;
                            $('.page1_page').animate({'left': times * 100 + '%' }, 500, function() {
                                app.isSelect = 0;
                                if(parseFloat($('.page1_page').css('left')) == -400) {
                                    that.fadeOut(150);
                                };
                            });
                        };
                    };
                });

                // 星球点击
                $('.planet').on('click', function() {
                    var that = $(this);
                    app.isSelect = 1;
                    app.planetNum = that.data('num');
                    switch(app.planetNum) {
                        case 1:
                            gtag('event', 'choose1', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 2:
                            gtag('event', 'choose2', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 3:
                            gtag('event', 'choose3', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 4:
                            gtag('event', 'choose4', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 5:
                            gtag('event', 'choose5', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                    };
                    if($('.page1_page').css('left') != 1 - app.planetNum + '00%') {
                        var t1 = new TimelineMax();
                        t1.to(context.$('.page1_page'), 0.5, { left: 1 - app.planetNum + '00%', onComplete: function() {
                            that.next().animate({'opacity': 1}, 150);
                            $('.buttons').fadeIn(150);
                            if( app.planetNum == 1) {
                                if($('.arrow.left').css('display') == 'block') {
                                    $('.arrow.left').fadeOut(150);
                                }
                            };
                            if( app.planetNum == 5) {
                                if($('.arrow.right').css('display') == 'block') {
                                    $('.arrow.right').fadeOut(150);
                                }
                            };
                        } }, '+=0.1');
                    } else {
                        if(that.next().css('opacity') == 0) {
                            that.next().animate({'opacity': 1}, 150);
                        };
                        if($('.buttons').css('display') == 'none') {
                            $('.buttons').fadeIn(150);
                        };
                    };
                    
                    // 滑动事件
                    // document.addEventListener('touchmove', defaultEvent, false);
                    // console.log(app.planetNum);
                });

                // 回到星空
                $('.back').on('click', function() {
                    app.isSelect = 0;
                    switch(app.planetNum) {
                        case 1:
                            gtag('event', 'back1', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 2:
                            gtag('event', 'back2', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 3:
                            gtag('event', 'back3', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 4:
                            gtag('event', 'back4', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                        case 5:
                            gtag('event', 'back5', {'event_category': 'genshenzhen', 'event_label': 'click' });
                            break;
                    };
                    app.planetNum = '';
                    $('.text').animate({'opacity': 0}, 300);
                    $('.buttons').fadeOut(150);
                    // 滑动事件
                    // document.removeEventListener('touchmove', defaultEvent, false);
                });

                // 选择星球
                $('.select').on('click', function() {
                    tl.kill();
                    $.ajax({
                        type: 'post',
                        url: app.baseUrl + '/genstar/CreateQrCode',
                        data: {
                            'openid': app.openid,
                            'code': app.planetNum
                        },
                        success: function(data) {
                            if(data.isSuccess) {
                                app.QeCodeId = data.QeCodeId;
                                app.router.goto('page2');
                            } else {
                                alert(data.Message);
                            }
                        },
                        error: function(data) {
                            alert('网络错误');
                        }
                    });
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {},
            //重力旋转
            DeviceMotion: function() {
                // 生成页面
                //运动事件监听
                if (window.DeviceMotionEvent) {
                    window.addEventListener('devicemotion', deviceMotionHandler, false);
                }

                //获取加速度信息
                //通过监听上一步获取到的x, y, z 值在一定时间范围内的变化率，进行设备是否有进行晃动的判断。
                //而为了防止正常移动的误判，需要给该变化率设置一个合适的临界值。
                var SHAKE_THRESHOLD = 4000;
                var last_update = 0;
                var x, y, z;

                function deviceMotionHandler(eventData) {
                    if(app.isSelect == 1) {
                        return;
                    };
                    var acceleration = eventData.accelerationIncludingGravity;
                    var curTime = new Date().getTime();
                    if ((curTime - last_update) > 10) {
                        var diffTime = curTime - last_update;
                        last_update = curTime;
                        x = acceleration.x;
                        y = acceleration.y;
                        z = acceleration.z;
                        var speed = Math.abs(x + y + z - app.last_x - app.last_y - app.last_z) / diffTime * 10000;
                        // if (speed > SHAKE_THRESHOLD) { 
                        // } 
                        app.last_x = x;
                        app.last_y = y;
                        app.last_z = z;
                        app.rotate = 6;
                        if (app.isiOS) {
                            starDirection('-400%', '0%', 400, 0, 40);
                        }
                        if (app.isAndroid) {
                            starDirection('0%', '-400%', 0, 400, 20);
                        }
                    }
                };

                //重力旋转
                function starDirection(left, right, distanceLeft, distanceRight, speed) {
                    var t2 = new TimelineMax({});
                    if (x > -0.4 && x < 0.4) {
                        // 停住
                        t2.pause();
                    } else if (x <= -0.4) {
                        // 向左
                        t2.to($('.page1_page'), Math.abs(parseFloat($('.page1_page').css('left')) + distanceLeft)/speed, { left: left, onUpdate: function() {
                            if(x > -1 && x < 1) {
                                t2.pause();
                            };
                            var currentLeft = parseFloat($('.page1_page').css('left'));
                            if(app.isiOS) {
                                if(currentLeft < -390) {
                                    $('.arrow.right').fadeOut(150);
                                };
                                if(currentLeft < -10) {
                                    if($('.arrow.left').css('display') == 'none') {
                                        $('.arrow.left').fadeIn(150);
                                    }
                                };
                            };
                            if(app.isAndroid) {
                                if(currentLeft > -10) {
                                    $('.arrow.left').fadeOut(150);
                                };
                                if(currentLeft > -390) {
                                    if($('.arrow.right').css('display') == 'none') {
                                        $('.arrow.right').fadeIn(150);
                                    }
                                };
                            };
                        }, onComplete: function() {
                            t2.kill();
                        } }, '+=0.1');
                    } else if (x >= 0.4) {
                        // 向右
                        t2.to($('.page1_page'), Math.abs(parseFloat($('.page1_page').css('left')) + distanceRight)/speed, { left: right, onUpdate: function() {
                            if(x > -1 && x < 1) {
                                t2.pause();
                            };
                            var currentLeft = parseFloat($('.page1_page').css('left'));
                            if(app.isiOS) {
                                if(currentLeft > -10) {
                                    $('.arrow.left').fadeOut(150);
                                };
                                if(currentLeft > -390) {
                                    if($('.arrow.right').css('display') == 'none') {
                                        $('.arrow.right').fadeIn(150);
                                    }
                                };
                            };
                            if(app.isAndroid) {
                                if(currentLeft < -390) {
                                    $('.arrow.right').fadeOut(150);
                                };
                                if(currentLeft < -10) {
                                    if($('.arrow.left').css('display') == 'none') {
                                        $('.arrow.left').fadeIn(150);
                                    }
                                };
                            };
                            
                        }, onComplete: function() {
                            t2.kill();
                        } }, '+=0.1');
                    };
                }
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })
