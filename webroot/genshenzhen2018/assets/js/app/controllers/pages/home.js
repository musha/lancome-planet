// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                // 获取授权
                app.localUrl = window.location.href;
                if(app.localUrl.indexOf('openid') < 0) {
                    location.href = app.baseUrl + '/interface/WeChatService.ashx?action=oauth&backurl=' + encodeURIComponent(app.localUrl);
                };
                app.openid = wxr_getParameterByName('openid');

                // 预加载
                var firstLoadFiles = {
                    "files": [
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/bg.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/product1.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/product2.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/product3.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/product4.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/product5.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page1/product5.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page2/bg.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/page3/bg.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/end/bg.png",
                            "size": 1
                        }
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        console.log(percentage);
                    }
                });

                // 适配
                $('.guide').css('top', $(window).height() * 0.946);
                $('.arrow').css('top', $(window).height() * 0.886);
                var distance = $('.arrow').css('top');

                var context = this;

                // 动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.home_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                    starTwinkle(context.$('.light1'));
                } }, 0.1);
                tl.to(context.$('.light2'), 0, { onComplete: function() {
                    starTwinkle(context.$('.light2'));
                } }, '+=0.5');
                tl.to(context.$('.arrow'), 0.6, { autoAlpha: 0, top: parseInt(distance) * 0.98 + '', repeat: -1, yoyo: true });

                // 星光闪耀
                function starTwinkle(el) {
                    var tl = new TimelineMax({repeat: -1, yoyo: true });
                    tl.to(el, 0.8, { autoAlpha: 0 });
                };

                // 滑动事件
                $('body').on('touchmove', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                });
                $('document').on('touchmove', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                });
                $('#main').on('touchmove', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                });
                var startx, starty;
                app.isHome = 1;
                // 获得角度
                function getAngle(angx, angy) {
                    return Math.atan2(angy, angx) * 180 / Math.PI;
                };
                // 根据起点终点返回方向 1向上 2向下 3向左 4向右 0未滑动
                function getDirection(startx, starty, endx, endy) {
                    var angx = endx - startx;
                    var angy = endy - starty;
                    var result = 0;
                    // 如果滑动距离太短
                    if (Math.abs(angx) < 2 && Math.abs(angy) < 2) {
                        return result;
                    }
                    var angle = getAngle(angx, angy);
                    if (angle >= -135 && angle <= -45) {
                        result = 1;
                    } else if (angle > 45 && angle < 135) {
                        result = 2;
                    } else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
                        result = 3;
                    } else if (angle >= -45 && angle <= 45) {
                        result = 4;
                    }
                    return result;
                };
                // 手指接触屏幕
                document.addEventListener('touchstart', function(e) {
                    startx = e.touches[0].pageX;
                    starty = e.touches[0].pageY;
                }, false);
                // 手指离开屏幕
                document.addEventListener('touchend', function(e) {
                    var endx, endy;
                    endx = e.changedTouches[0].pageX;
                    endy = e.changedTouches[0].pageY;
                    var direction = getDirection(startx, starty, endx, endy);

                    if(direction == 1) {
                        gtag('event', 'slip', {'event_category': 'genshenzhen', 'event_label': 'click' });
                        tl.kill();
                        if(app.isHome == 1) {
                            app.router.goto('page1');
                            app.isHome = 0;
                        }
                    }
                }, false);

                // 点击事件
                $('.arrow').on('click', function() {
                    gtag('event', 'slip', {'event_category': 'genshenzhen', 'event_label': 'click' });
                    tl.kill();
                    if(app.isHome == 1) {
                        app.router.goto('page1');
                        app.isHome = 0;
                    }
                });
                $('.guide').on('click', function() {
                    gtag('event', 'slip', {'event_category': 'genshenzhen', 'event_label': 'click' });
                    tl.kill();
                    if(app.isHome == 1) {
                        app.router.goto('page1');
                        app.isHome = 0;
                    }
                });
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })