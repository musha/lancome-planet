// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min", "vendor/zepto/fx", "vendor/zepto/fx_methods", , "vendor/md5"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                gtag('config', 'UA-84868999-18', {'page_title': 'genshenzhen_p4' }); 

                // 暂时取消滑动事件
                // $('body').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                // $('document').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                // $('#main').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.page3_page'), 0.2, { autoAlpha: 0 }, 0.1);

                // 倒计时
                var countTime = 10;
                function countDown() {
                    if (countTime == 0) {  
                        $('.send').removeAttr('disabled');  
                        $('.send').text('获取验证码');  
                        countTime = 10;  
                        return false;  
                    } else {  
                        $('.send').attr('disabled', true);  
                        $('.send').text('重新发送(' + countTime + ')');  
                        countTime--;  
                    };  
                    setTimeout(function() {  
                        countDown($('.send'));  
                    },1000);
                };
                // 发送验证码
                $('.send').on('click', function() {
                    if($('input[name="mobile"]').val() == '') {
                        alert('请输入您的手机号');
                        return;
                    } else {
                        if(!(/^1\d{10}$/.test($('input[name="mobile"]').val()))) {
                            alert('请输入正确的手机号码');
                            return;
                        }
                    };
                    var timestamp = new Date().getTime();
                    var sign = $('input[name="mobile"]').val() + timestamp;
                    $.ajax({
                        type: 'post',
                        url: app.baseUrl + '/genstar/SendCode',
                        data: {
                            'mobile': $('input[name="mobile"]').val(),
                            'timestamp': timestamp,
                            'sign': hex_md5(sign)
                        },
                        success: function(data) {
                            if(data.isSuccess) {
                                countDown();
                            };
                            alert(data.Message);
                        },
                        error: function(data) {
                            alert('网络错误');
                        }
                    });
                });

                // 提交
                $('.btn.submit').on('click', function() {
                    gtag('event', 'submit', {'event_category': 'genshenzhen', 'event_label': 'click' });

                    if($('input[name="name"]').val() == '') {
                        alert('请输入您的姓名');
                        return;
                    };
                    if($('input[name="mobile"]').val() == '') {
                        alert('请输入您的手机号');
                        return;
                    };
                    if($('input[name="code"]').val() == '') {
                        alert('请输入验证码');
                        return;
                    }
                    $.ajax({
                        type: 'post',
                        url: app.baseUrl + '/genstar/Apply',
                        data: {
                            'openid': app.openid,
                            'mobile': $('input[name="mobile"]').val(),
                            'code': $('input[name="code"]').val(),
                            'name': $('input[name="name"]').val()
                        },
                        success: function(data) {
                            if(data.isSuccess) {
                                gtag('config', 'UA-84868999-18', {'page_title': 'genshenzhen_p5' }); 
                                $('.modal').fadeIn('fast');
                            } else {
                                alert(data.Message);
                            }
                        },
                        error: function(data) {
                            alert('网络错误');
                        }
                    });
                });
                
                // 弹窗按钮
                $('.go').on('click', function() {
                    window.location.href = 'https://www.lancome.com.cn';
                });
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })