// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                gtag('config', 'UA-84868999-18', { 'page_title': 'genshenzhen_p3' });

                // 暂时取消滑动事件
                // $('body').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                // $('document').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                // $('#main').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                
                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.page2_page'), 0.2, { autoAlpha: 0 }, 0.1);

                //二维码
                var qrcode = new QRCode('qrcode', {
                    text: 'openid=' + app.openid +'&code=' + app.planetNum,
                    width: $('#qrcode').width(),
                    height: $('#qrcode').height(),
                    colorDark : '#000',
                    colorLight : 'transparent',
                    correctLevel : QRCode.CorrectLevel.H
                });

                // 检测是否扫过二维码
                var isScan = setInterval(function() {
                    $.ajax({
                        type: 'post',
                        url: app.baseUrl + '/genstar/VerifyQrCode',
                        data: {
                            'openid': app.openid,
                            'code': app.planetNum
                        },
                        success: function(data) {
                            if(data.isSuccess) {
                                if(data.ApplyStatus == 0) {
                                    clearInterval(isScan);
                                    app.router.goto('page3');
                                } else {
                                    clearInterval(isScan);
                                    app.router.goto('end');
                                };
                            } else {
                                // alert(data.Message);
                                // clearInterval(isScan);
                            }
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }, 2000);
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })