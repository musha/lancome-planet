// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                gtag('config', 'UA-84868999-18', {'page_title': 'genshenzhen_p6' }); 
                
                // 暂时取消滑动事件
                // $('body').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                // $('document').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });
                // $('#main').on('touchmove', function(event) {
                //     event.preventDefault();
                //     event.stopPropagation();
                // });

                
                // 调整手机屏幕尺寸 
                $('.btn.go').css('top', $(window).height() * 0.9);
                

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.end_page'), 0.2, { autoAlpha: 0 }, 0.1);
                // tl.to(context.$('.arrow'), 0.6, { autoAlpha: 0, top: '87.2%', repeat: -1, yoyo: true });

                // 去到官网
                $('.btn.go').on('click', function() {
                    window.location.href = 'https://www.lancome.com.cn';
                });
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })