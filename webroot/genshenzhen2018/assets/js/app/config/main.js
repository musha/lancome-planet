require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        // app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);

        // 接口地址
        app.baseUrl = 'http://campaign.lancome.com.cn/genshenzhen2018';

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });

        // 微信不分享
        // function onBridgeReady() { 
        //     WeixinJSBridge.call('hideOptionMenu'); 
        // };
        // if (typeof WeixinJSBridge == "undefined") { 
        //     if (document.addEventListener) { 
        //         document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false); 
        //     } else if (document.attachEvent) { 
        //         document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        //         document.attachEvent('onWeixinJSBridgeReady', onBridgeReady); 
        //     } 
        // } else { 
        //     onBridgeReady(); 
        // };

        //微信分享

        var init = {};

        init.data = {
            share: {
                moment: {
                    title: '兰蔻寻光之旅',
                    desc: '即刻前往深圳茂业天地，开启你的寻光之旅！',
                    imgUrl: 'http://campaign.lancome.com.cn/genshenzhen2018/webroot/genshenzhen2018/assets/images/share.jpg',
                    link: 'http://campaign.lancome.com.cn/genshenzhen2018/webroot/genshenzhen2018/index.html'
                },
                friend: {
                    title: '兰蔻寻光之旅',
                    desc: '即刻前往深圳茂业天地，开启你的寻光之旅！',
                    imgUrl: 'http://campaign.lancome.com.cn/genshenzhen2018/webroot/genshenzhen2018/assets/images/share.jpg',
                    link: 'http://campaign.lancome.com.cn/genshenzhen2018/webroot/genshenzhen2018/index.html'
                }
            }
        };

        (function() {
            //正式环境获取微信接口
            $.ajax({
                url:app.baseUrl + '/Interface/WeChatService.ashx?action=jssdk',
                data: { url: window.location.href },
                dataType: 'json',
                success: function(config) {
                    var wxConfig = {
                        debug: false,
                        appId: config.AppId,
                        timestamp: config.Timestamp,
                        nonceStr: config.NonceStr,
                        signature: config.Signature,
                        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo']
                    };
                    wx.config(wxConfig);
                },
                error:function(config) {
                    console.log(config);
                    /* Act on the event */
                }
            });

            //绑定微信分享事件
            try {
                wx.ready(function() {

                    //设置分享后的回调函数
                    var callback = {
                        success: function(msg) {
                            msg = JSON.stringify(msg).toLowerCase();
                            if (msg.indexOf('timeline') > -1) {
                                //分享到朋友圈
                            } else if (msg.indexOf('message') > -1) {
                                //分享到给朋友
                            }
                        }
                    };
                    wx.onMenuShareTimeline($.extend({}, init.data.share.moment, callback));
                    wx.onMenuShareAppMessage($.extend({}, init.data.share.friend, callback));
                });

            } catch (e) {
                var msg = '错误：微信js-sdk未引用或者错误!';
                try {
                    console.log(msg);
                } catch (e) {
                    alert(msg);
                }
                return;
            }
        })();
    });