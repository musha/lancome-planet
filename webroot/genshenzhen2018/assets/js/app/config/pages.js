define([
        // "controllers/pages/loading",
        "controllers/pages/home",
        "controllers/pages/page1",
        "controllers/pages/page2",
        "controllers/pages/page3",
        "controllers/pages/end"
    ],


    function(Home, Page1, Page2, Page3, End) {

        var pages = [{
                routeId: 'home',
                type: 'main',
                landing: true,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            }, {
                routeId: 'page1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page1.View({ template: "pages/page1" });
                }
            },
            {
                routeId: 'page2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page2.View({ template: "pages/page2" });
                }
            },
            {
                routeId: 'page3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page3.View({ template: "pages/page3" });
                }
            },
            {
                routeId: 'end',
                type: 'main',
                landing: false,
                page: function() {
                    return new End.View({ template: "pages/end" });
                }
            }

        ];


        return pages;

    });