/* global module */
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        requirejs: {
            default: {
                options: {
                    name: "main",
                    wrapShim: true,
                    baseUrl: "../webroot/genshenzhen2018/assets/js/app",
                    mainConfigFile: "../webroot/genshenzhen2018/assets/js/app/config/config.js",
                    out: "../webroot/genshenzhen2018/assets/js/_app.js",
                    insertRequire: ['main'],
                    preserveLicenseComments: false
                }
            }

        },

        jst: {
            default: {
                options: {
                    templateSettings: {
                        interpolate: /\{\{(.+?)\}\}/g
                    },
                    prettify: true,
                    processName: function(filepath) {

                        var prefix = '../webroot/genshenzhen2018/assets/js/app/views/';
                        var suffix = '.html';

                        if (filepath.indexOf(prefix) == 0) {
                            filepath = filepath.substring(prefix.length);
                        }

                        if (filepath.lastIndexOf(suffix) == filepath.length - suffix.length) {
                            filepath = filepath.substring(0, filepath.length - suffix.length);
                        }

                        return filepath;
                    }
                },
                files: {
                    "../webroot/genshenzhen2018/assets/js/_views.js": ["../webroot/genshenzhen2018/assets/js/app/views/**/*.html"]
                }
            }
        },

        concat: {
            options: {
                separator: ';\n\n'
            },
            dist: {
                src: ['../webroot/genshenzhen2018/assets/js/app/vendor/base/require.js', '../webroot/genshenzhen2018/assets/js/_views.js', '../webroot/genshenzhen2018/assets/js/_app.js'],
                dest: '../webroot/genshenzhen2018/assets/js/app.js'
            }
        },

        less: {
            default: {
                options: {
                    //paths: ["assets/css"],
                    cleancss: true,
                    modifyVars: {
                        //imgPath: '"http://mycdn.com/path/to/images"',
                        //bgColor: 'red'
                    }
                },
                files: {
                    "../webroot/genshenzhen2018/assets/css/style.css": "../webroot/genshenzhen2018/assets/css/style.less"
                }
            }
        },

        clean: {
            options: { force: true },
            default: ["../webroot/genshenzhen2018/assets/js/_app.js", "../webroot/genshenzhen2018/assets/js/_views.js"]
        },


        tmtTinyPng: {
            default_options: {
                options: {},
                files: [{
                    expand: true,
                    src: ['*.png', '*.jpg', '*/*.png', '*/*.jpg'],
                    cwd: '../webroot/genshenzhen2018/assets/images',
                    filter: 'isFile'
                }]
            }
        }


    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jst');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-css-url-embed');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-tmt-tiny-png');


    grunt.registerTask('default', ['requirejs', 'jst', 'concat', 'less', 'clean']);

};